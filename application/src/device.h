/*
 * RoboPlay for MSX
 * Copyright (C) 2022 by RoboSoft Inc.
 * 
 * devices.h
 */

#pragma once

#include <stdint.h>

#include "drivers/inc/opl4.h"
#include "drivers/inc/scc.h"
#include "drivers/inc/opm.h"
#include "drivers/inc/psg.h"
#include "drivers/inc/midi-pac.h"

#include "players/inc/player_interface.h"

void init_devices();
void reset_devices();
void restore_devices();

void set_opl_mode(const roboplay_opl_mode mode);

void write_opl_fm_1(const uint8_t reg, const uint8_t value);
void write_opl_fm_2(const uint8_t reg, const uint8_t value);

void write_opm_fm(const uint8_t reg, const uint8_t value);

void set_refresh();