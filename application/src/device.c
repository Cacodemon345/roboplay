/*
 * RoboPlay for MSX
 * Copyright (C) 2022 by RoboSoft Inc.
 * 
 * devices.c
 */

#include "support/inc/printf_simple.h"

#include "device.h"
#include "dos.h"

#define OPL4_TIMER1_COUNT  0x02
#define OPL4_TIMER2_COUNT  0x03
#define OPL4_TIMER_CONTROL 0x04

/*
 * init_opl4_device
 */
void init_opl4_device()
{
    printf_simple("\n\rDetecting OPL4 device");

    if (!opl4_detect())
    {
        printf_simple(" ...Not Found\n\r");
        dos_exit(0);
    }
    else
    {
        opl4_reset();
        printf_simple(" ...Found, sample memory: %iK\n\r", opl4_sample_ram_banks() * 64);
    }
}

/*
 * init_scc_device
 */
void init_scc_device()
{
  printf_simple("Detecting SCC  device");

  if(!scc_find_slot())
  {
    printf_simple(" ...Not Found\n\r");
  }
  else
  {
    uint8_t scc_slot = scc_get_slot();
    if(scc_slot & 0x80)
      printf_simple(" ...Found, located in slot %i-%i\n\r", scc_slot & 0x03, (scc_slot & 0x0C) >> 2);
    else
      printf_simple(" ...Found, located in slot %i\n\r", scc_slot);

    scc_reset();
  }
}

/*
 * init_opm_device
 */
void init_opm_device()
{
  printf_simple("Detecting OPM  device");

  if(!opm_find_slot())
  {
    printf_simple(" ...Not Found\n\r");
  }
  else
  {
    uint8_t opm_slot = opm_get_slot();
    if(opm_slot & 0x80)
      printf_simple(" ...Found, located in slot %i-%i\n\r", opm_slot & 0x03, (opm_slot & 0x0C) >> 2);
    else
      printf_simple(" ...Found, located in slot %i\n\r", opm_slot);

    opm_reset();
  }
}

/*
 * init_midi_device
 */
void init_midi_device()
{
  printf_simple("Detecting MIDI device");

  if(midi_pac_detect())
  {
    printf_simple(" ...Found, MIDI-PAC\n\r");
  }
  else
  {
    printf_simple(" ...Not Found\n\r");
  }
}

void init_devices()
{
  init_opl4_device();
  init_scc_device();
  init_opm_device();
  init_midi_device();

  reset_devices();
}

void reset_devices()
{
  opl4_reset();
  opm_reset();
  scc_reset();
  psg_reset();
  midi_pac_reset();
}

void restore_devices()
{
  midi_pac_restore();
}

void set_opl_mode(const roboplay_opl_mode mode)
{
    switch(mode)
    {
        case ROBOPLAY_OPL_MODE_OPL2:
            opl4_write_fm_register_array_2(0x05, 0x00);
            break;
        case ROBOPLAY_OPL_MODE_OPL3:
            opl4_write_fm_register_array_2(0x05, 0x01);
            break;
        case ROBOPLAY_OPL_MODE_OPL4:
            opl4_write_fm_register_array_2(0x05, 0x03);
            break;
    }
}

void write_opl_fm_1(const uint8_t reg, const uint8_t value)
{
#ifdef __DEBUG
    printf_simple("1:%x:%x ", reg, value);
#endif

    if(reg == 2 || reg == 3 || reg == 4) return;

    opl4_write_fm_register_array_1(reg, value);
}

void write_opl_fm_2(const uint8_t reg, const uint8_t value)
{
#ifdef __DEBUG
    printf_simple("2:%x:%x ", reg, value);
#endif

    opl4_write_fm_register_array_2(reg, value);
}

void write_opm_fm(const uint8_t reg, const uint8_t value)
{
    if(reg == 0x11 || reg == 0x12 || reg == 0x13 || reg == 0x14) return;

    opm_write_register(reg, value);
}

void set_refresh()
{
    float refresh = g_roboplay_interface->get_refresh();

#ifndef __DEBUG    
    if (refresh > 50.0)
    {
        opl4_write_fm_register_array_1(OPL4_TIMER1_COUNT, 255 - (uint8_t)((1000.0 / refresh) / 0.08));
        opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x21);
        opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x80);
    }
    else
    {
        opl4_write_fm_register_array_1(OPL4_TIMER2_COUNT, 256 - (uint8_t)((1000.0 / refresh) / 0.320));
        opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x42);
        opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x80);
    }
#else
    printf_simple("\n\rRefresh: %i\n\r", (uint16_t)refresh);
#endif
}
