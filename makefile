include config.env

all: application

export PATH := $(PWD)\bin:$(PATH)

support:
	make -C support

drivers:
	make -C drivers

players:
	make -C players

data:
	make -C data

application: dsk support drivers data players
	make -C application

dsk:
	mkdir $(OUTPUT)\$(DISK)

.PHONY: clean cleanall support drivers players data application
clean:
	@make -C support clean
	@make -C drivers clean
	@make -C players clean
	@make -C data clean
	@make -C application clean
	@if exist $(OUTPUT) del /s/q $(OUTPUT)
	@if exist $(OUTPUT) rmdir $(OUTPUT)

